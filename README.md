# TAF Cucumber - STANDARD ZERO FRAMEWORK TEMPLATE for BDD
Example of ready to use Cucumber project

# TODO
* This Standard Zero Template contains test examples for REST, SOAP and UI with Feature files.
* If you are using this framework for SOAP tests, please remove REST and UI test and feature examples.
* Rename `TestStepsSOAP.java` to `TestSteps.java`
* Remove unnecessary dependencies from `build.gradle`

# Examples of usage
To run smoke test suite
```bash
gradle task smoke
```

To run regression test suite
```bash
gradle task regression
```

# Properties of smoke or regression task
To configure the Cucumber BDD test task you can choose to set the following properties using:

* `-DprojectIp` : Specified the projectIP of the server

We can run tests with projectIp property:
```bash
gradle task smoke -DprojectIp="http://10.160.7.108"
```
```bash
gradle task regression -DprojectIp="http://10.160.7.108"
```

# Feature files, Test Steps and Local Helpers
* All feature files should be located under `/src/test/resources/Features`
* All TestSteps.java classes should be located under `/src/test/java/com.datalex.taf.cucumber.stepDefinition`
* All local helpers should be located under `/src/main/java/com.datalex.taf`
* All temp files should be in `/work/`

# Setting new project:
* Change project name in `settings.gradle`
* Change `rootProject.name = 'taf-cucumber'`
* Change description in `build.gradle`
* Change `description = "cucumber"` to match team name `description = "lh-checkoutandshoppingbasket"`

# In `project.properties` configuration file change following:
* Change `projectName=PAL`
* Change `customerSpecificControls=true`
* Change `customerSpecificPages=true`
* Change `customerCode=103`
* Change `pos=PALCUI_SABRE`
* Change `projectIp=http://palpst2.datalex.com`

# In `taf.properties` configuration file change following:
* Change `browserType=firefox`
* Change `gridHost=http://10.153.30.103` - if running locally set to `gridHost=http://localhost`
* Change `gridPort=4444`

# Reporting
* Reports are located in: `/target/cucumber-html-reports/index.html`

# Running UI Tests locally:
* Download JAR [selenium-server-standalone-2.53.0.jar](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-server-standalone/2.53.0)
* Run following command in command line to start Selenium hub:
```bash
java -jar selenium-server-standalone-2.53.0.jar -role hub
```
* Run following command in command line to start Selenium node:
```bash
java -jar selenium-server-standalone-2.53.0.jar -role webdriver
```
* NOTE: Do NOT use latest version of Firefox!