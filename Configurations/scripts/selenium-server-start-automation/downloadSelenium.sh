#!/usr/bin/env bash

password='@ut0p$t'
SELENIUM_VERSION='2.53.1'
DOWNLOAD_COMMAND="wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-$SELENIUM_VERSION.jar"
RENAME_COMMAND="mv selenium-server-standalone-$SELENIUM_VERSION.jar selenium-server-standalone.jar"
seleniumServerPath="/home/pstauto/selenium-server-standalone.jar"
KILL_NODES='kill -9 $(lsof -t -i:5555)'

if [ ! -f $seleniumServerPath ]; then $DOWNLOAD_COMMAND;
    else echo "Selenium Server is present!: $seleniumServerPath . . ."
fi


  echo $password | sudo -S apt-get -y install openjdk-7-jre-headless



  echo $password | sudo -S apt-get -y install xvfb



  echo $password | sudo -S apt-get -y install xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic


echo "Renaming to selenium-server-standalone.jar......................................"
$RENAME_COMMAND

echo "Setting selenium node service .................................................."
echo $password | sudo -S mv /home/pstuser/selenium-node.conf /etc/init/
echo "Start selenium node service ...................................................."
echo $password | sudo -S service selenium-node start
echo "FINISH..................... ...................................................."