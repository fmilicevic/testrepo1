#!/usr/bin/env bash
echo
echo ____________________________________
echo
echo " Starting Automation Environment"
echo ____________________________________
echo
user='pstuser'
password='@ut0p$t'

SELENIUM_VERSION='2.53.1'
DOWNLOAD_COMMAND="wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-$SELENIUM_VERSION.jar"
RENAME_COMMAND="mv selenium-server-standalone-$SELENIUM_VERSION.jar selenium-server-standalone.jar"
KILL_NODES='kill -9 $(lsof -t -i:4444)'
DOWNLOAD_SELENIUM_CHMOD="chmod 777 downloadSelenium.sh"
DOWNLOAD_SELENIUM="sh ./downloadSelenium.sh"
RUN_NODE_SCRIPT_PATH="/home/pstuser/downloadSelenium.sh"
CONF_PATH="/home/pstuser/selenium-node.conf"
COPY_CONF="sudo cp /home/pstuser/selenium-node.conf /etc/init/ < $password"
SCRIPT_DESTINATION_PATH="/home/pstuser/"

seleniumServerPath="/home/pstauto/selenium-server-standalone.jar"
if [ ! -f $seleniumServerPath ]; then $DOWNLOAD_COMMAND;
    else echo "Selenium Server is present!: $seleniumServerPath . . ."
fi

echo "Renaming to selenium-server-standalone.jar..."
$RENAME_COMMAND

echo "Checking for "sshpass" package..."
if ! dpkg -s sshpass > /dev/null; then
  echo -e "sshpass is not installed! Installing now..."
  echo $password | sudo -S apt-get -y install sshpass
  else echo "sshpass is installed!"
fi


  echo $password | sudo -S apt-get -y install openjdk-7-jre-headless


  echo $password | sudo -S apt-get -y install xvfb


  echo $password | sudo -S apt-get -y install xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic


echo "Setting selenium node service .................................................."
echo $password | sudo -S mv /home/pstuser/selenium-hub.conf /etc/init/

#echo "STOPPING HUB..."
#echo $password | sudo -S service selenium-hub stop

echo "STARTING HUB..."
echo $password | sudo -S service selenium-hub start

NODE5="10.153.30.108"
NODE6="10.153.30.109"
NODE7="10.153.30.110"
NODE8="10.153.30.111"

echo "###################### START NODE 5 - $NODE5 - ########################################################"
echo "Copy downloadSelenium to $NODE5.................................................................."
sshpass -p $password scp -r $RUN_NODE_SCRIPT_PATH $user@$NODE5:$SCRIPT_DESTINATION_PATH
echo "Copy selenium-hub.conf to $NODE5..........................................................."
sshpass -p $password scp -r $CONF_PATH $user@$NODE5:$SCRIPT_DESTINATION_PATH
echo "Running downloadSelenium.sh on $NODE5..............................................................."
sshpass -p $password ssh -t -o StrictHostKeyChecking=no $user@$NODE5 $DOWNLOAD_SELENIUM
echo "###################### FINISH NODE 5 - $NODE5 - ########################################################"

echo "###################### START NODE 6 - $NODE6 - ########################################################"
echo "Copy downloadSelenium to $NODE6.................................................................."
sshpass -p $password scp -r $RUN_NODE_SCRIPT_PATH $user@$NODE6:$SCRIPT_DESTINATION_PATH
echo "Copy selenium-hub.conf to $NODE6..........................................................."
sshpass -p $password scp -r $CONF_PATH $user@$NODE6:$SCRIPT_DESTINATION_PATH
echo "Running downloadSelenium.sh on $NODE6..............................................................."
sshpass -p $password ssh -t -o StrictHostKeyChecking=no $user@$NODE6 $DOWNLOAD_SELENIUM
echo "###################### FINISH NODE 6 - $NODE6 - ########################################################"

echo "###################### START NODE 7 - $NODE7 - ########################################################"
echo "Copy downloadSelenium to $NODE7.................................................................."
sshpass -p $password scp -r $RUN_NODE_SCRIPT_PATH $user@$NODE7:$SCRIPT_DESTINATION_PATH
echo "Copy selenium-hub.conf to $NODE7..........................................................."
sshpass -p $password scp -r $CONF_PATH $user@$NODE7:$SCRIPT_DESTINATION_PATH
echo "Running downloadSelenium.sh on $NODE7..............................................................."
sshpass -p $password ssh -t -o StrictHostKeyChecking=no $user@$NODE7 $DOWNLOAD_SELENIUM
echo "###################### FINISH NODE 7 - $NODE7 - ########################################################"

echo "###################### START NODE 8 - $NODE8 - ########################################################"
echo "Copy downloadSelenium to $NODE8.................................................................."
sshpass -p $password scp -r $RUN_NODE_SCRIPT_PATH $user@$NODE8:$SCRIPT_DESTINATION_PATH
echo "Copy selenium-hub.conf to $NODE8..........................................................."
sshpass -p $password scp -r $CONF_PATH $user@$NODE8:$SCRIPT_DESTINATION_PATH
echo "Running downloadSelenium.sh on $NODE8..............................................................."
sshpass -p $password ssh -t -o StrictHostKeyChecking=no $user@$NODE8 $DOWNLOAD_SELENIUM
echo "###################### FINISH NODE 8 - $NODE8 - ########################################################"