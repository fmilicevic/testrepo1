#!/bin/bash



function entryMessage {
  echo "########################################################################################"
  echo "########################################################################################"
  echo "######################                                         #########################"
  echo "######################           INSTALL GECKODRIVER           #########################"
  echo "######################                                         #########################"
  echo "########################################################################################"
  echo "########################################################################################"
  echo
}

function voidMain {
  entryMessage
  local VERISON_X="INVALID"
  until [ "$VERISON_X" == "VALID" ]
  do
    echo -n "Please enter your desired version for Gecko driver for installation : "
    read GECKO_VERSION
    if [ ! -z "$GECKO_VERSION" ]
    then
      VALIDITY_CHECK="VALID"
      GECKO_NAME=geckodriver-$GECKO_VERSION-linux64.tar.gz
      TMP_PATH=$PWD
      mkdir /home/$USER/geckodriver
      cd /home/$USER/geckodriver
      wget https://github.com/mozilla/geckodriver/releases/download/$GECKO_VERSION/$GECKO_NAME
      tar -xvzf $GECKO_NAME
      rm $GECKO_NAME
      chmod +x geckodriver

      echo "if [ -f $TMP_PATH/.geckoPath ]; then . $TMP_PATH/.geckoPath; fi" >> ~/.bashrc
      onDestroy 0
    else
      echo -e "\n!!! Version is not recognized. Try again !!!\n"
    fi
  done
}

function onDestroy {
  if [ $1 -eq 0 ]
  then
    echo "The script has successfully installed gecko driver"
    exit 0
  elif [ $1 -eq 2 ]
  then
    echo "The script ended."
    exit 0
  else
    echo "The script failed."
    exit 1
  fi
}

function onCreate {
  voidMain
}

function ctrl_c {
  echo -e "\nUser terminated"
  onDestroy 1
}

trap ctrl_c SIGINT

if [[ "$PATH" == *"/geckodriver"* ]]
then
  echo "Geckodriver it's already installed and included in PATH!";
  onDestroy 2
else
  onCreate
fi

onDestroy 0
