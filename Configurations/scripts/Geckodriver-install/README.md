How to install Geckodriver
==========================

In directory Geckodriver-install run:

./install_geckodriver.sh

Then script ask you which version of gecko driver do you want install.

Example (latest): v0.11.1

Press ENTER and geckodriver will be donwloaded then after whole proccess you must re-open terminal.

You can check if installation was sucessfull with command:

geckodriver
