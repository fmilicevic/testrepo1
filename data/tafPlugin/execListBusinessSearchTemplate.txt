TestCase	This script will test if you can book and pay a RT for 1ADT with VISA	TC_ONE				
Selenium.deleteCookies	3					
Selenium.openURL	3	http://MapValue:projectIp:MapValue/%urlAction%/ApplicationStartAction.do
Selenium.typeFlight	3	inputFrom	%routeFrom%			
Selenium.typeFlight	3	inputTo	%routeTo%			
Selenium.inputDateByCalendar	3	inputDepartOn	%departOn%
Selenium.inputDateByCalendar	3	inputReturnOn	%returnOn%			
Selenium.selectOption	3	selectADTQuantity	2			
Selenium.selectOption	3	selectCNNQuantity	2			
Selenium.selectOption	3	selectINFQuantity	2			
Selenium.selectOptionByValue	4	selectCabinClass	Business			
Selenium.selectOptionByValue	4	selectCabinClass	BusinessFirst			
Selenium.clickAndWait	3	buttonSearch				
Selenium.waitPause	3	8000				