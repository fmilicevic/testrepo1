SELECT loc.code as airCodeCount, locname.name
FROM location loc, locationname locname 
WHERE loc.locationtypecode='A'
AND locname.locationid = loc.id
AND locname.name like('%ew Y%')
and locname.languageid in (select lang.id
from language lang
where lang.code='en')