package com.datalex.taf.cucumber.stepDefinition;



import com.datalex.taf.core.readers.property.TAFProperties;
import com.datalex.taf.ui.TAFSelenium;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestStepsUI {
    private static TAFSelenium driver;

    @Before
    public void setUp() throws Throwable {
        driver = new TAFSelenium();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Given("^User has populated FlightSearch Page$")
    public void userHasPopulatedFlightSearchPage() throws Throwable {
        String dynUrl = TAFProperties.getPROJECTIP() + "/flypal/ApplicationStartAction.do?pos=" + TAFProperties.getPOS();
        driver.open(dynUrl);
        driver.clickInput("radioRoundTrip");
        driver.selectOptionByValue("selectFrom", "MNL");
        driver.selectOptionByValue("selectTo", "DVO");
        driver.inputDateByCalendar("inputDepartOn", "16");
        driver.inputDateByCalendar("inputReturnOn", "27");
        driver.clickInput("radioTravelDatesFlexible");
        driver.saveSearchData();
        driver.clickAndWait("buttonSearch");
        driver.waitPause(10000);
    }

    @And("^User populates Selection Page$")
    public void userPopulatesSelectionPage() throws Throwable {
        driver.waitForElement("buttonNext");
        driver.clickAndWait("buttonNext");
        driver.waitPause(10000);
    }

    @And("^User populates Summary Page$")
    public void userPopulatesSummaryPage() throws Throwable {
        driver.waitForElement("taxTermsAndCons");
        driver.clickInput("taxTermsAndCons");
        driver.waitForElement("buttonNext");
        driver.click("buttonNext");
        driver.waitPause(10000);
    }

    @And("^User populates Travellers Page$")
    public void userPopulatesTravellersPage() throws Throwable {
        driver.fillPage("Travellers");
        driver.waitForElement("paymentModeCreditCardRadio");
        driver.clickInput("paymentModeCreditCardRadio");
    }

    @When("^User purchase flight$")
    public void userPurchaseFlight() throws Throwable {
        driver.waitPause(5000);
        driver.clickAndWait("id=pgButtonProceed");
        driver.waitPause(60000);
        driver.fillPage("PAYMENTSMASTER");
        driver.waitPause(5000);
        driver.clickAndWait("buttonPaymentPurchase");
    }

    @Then("^User get Confirmation Page$")
    public void userGetConfirmationPage() throws Throwable {
        driver.waitPause(20000);
        driver.saveReservationNumber();
    }
}
