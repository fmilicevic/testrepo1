package com.datalex.taf.cucumber.stepDefinition;

import com.datalex.taf.businessObjects.FlightInformation;
import com.datalex.taf.core.loggers.TAFLogger;
import com.datalex.taf.core.readers.ResourceFileReader;
import com.datalex.taf.rest.helpers.ActiveJSONObject;
import com.datalex.taf.rest.helpers.RESTSender;
import com.datalex.taf.rest.helpers.TDPRESTService;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.testng.Assert;

import java.io.File;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestStepsREST {
    private static String uri;
    private FlightInformation flightInformation = new FlightInformation();
    private ResourceFileReader resourceFileReader = new ResourceFileReader();

    @Given("^URI is \"([^\"]*)\"$")
    public void uriIs(String uri) throws Throwable {
        TestStepsREST.uri = uri;
    }

    @Given("^User has flight from (.*) to (.*) (.*) days from today$")
    public void userHasFlightFromFromToTo(String from, String to, String daysFromToday) throws Throwable {
        flightInformation.setFrom(from);
        flightInformation.setTo(to);
        flightInformation.setDaysFromToday(Integer.parseInt(daysFromToday));
    }

    @When("^User send LFS request$")
    public void userSendLFSRequest() throws Throwable {
        String stringRequest = resourceFileReader.getResourcesFileText("requests/LFSTestPostSample.json");

        DateTime futureDate = new DateTime().plusDays(flightInformation.getDaysFromToday());
        String stringFutureDate = futureDate.toString("YYYY-MM-dd");
        ActiveJSONObject lfsObj = new ActiveJSONObject(stringRequest);
        lfsObj.changeJSONParamInRQ("originDestination/origin/date", stringFutureDate);
        lfsObj.changeJSONParamInRQ("originDestination/origin/location/code", flightInformation.getFrom());
        lfsObj.changeJSONParamInRQ("originDestination/destination/location/code", flightInformation.getTo());

        TDPRESTService lfsService = new TDPRESTService();
        lfsService.setAppURL(uri);
        lfsService.setServiceName("/air/lfs");

        RESTSender sender = new RESTSender().xmlToBeSent(false);
        ClientResponse response = sender.post(lfsService, lfsObj.toString());

        String jsonResponseString = response.getEntity(String.class);
        TAFLogger.debug("String representation:");
        TAFLogger.debug(jsonResponseString);

        File file = new File("./work/lfsResponse.json");
        FileUtils.writeStringToFile(file, jsonResponseString);
    }

    @Then("^Verify response")
    public void verifyResponse() throws Throwable {
        File file = new File("./work/lfsResponse.json");
        String stringResponse = FileUtils.readFileToString(file);
        ActiveJSONObject responseObj = new ActiveJSONObject(stringResponse);
        Assert.assertTrue(responseObj.verifyJXpathExist("pos"));
    }
}
