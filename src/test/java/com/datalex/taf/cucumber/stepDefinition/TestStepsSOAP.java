package com.datalex.taf.cucumber.stepDefinition;

import com.datalex.taf.businessObjects.UserInformation;
import com.datalex.taf.soap.converters.ISOAPMessageConverter;
import com.datalex.taf.soap.converters.SOAPMessageConverter;
import com.datalex.taf.soap.factories.ISOAPMessageFactory;
import com.datalex.taf.soap.factories.SOAPMessageFactory;
import com.datalex.taf.soap.helpers.SOAPMessageHelper;
import com.datalex.taf.soap.sender.ISOAPSender;
import com.datalex.taf.soap.sender.SOAPSender;
import com.datalex.taf.transformations.BooleanTransformation;

import org.junit.Assert;
import org.w3c.dom.Document;

import java.util.List;

import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestStepsSOAP {
    private static UserInformation userInformation;
    private static String url;
    private ISOAPMessageFactory messageFactory = new SOAPMessageFactory();
    private SOAPMessageHelper soapMessageHelper = new SOAPMessageHelper();
    private Document response;


    @Given("^Url is \"(.*)\"")
    public void storeUrl(String url) {
        this.url = url;
    }

    @Given("^User has entered his reservation details.")
    public void enterUserDetails(List<UserInformation> userDetails) throws Exception {
        if (!userDetails.isEmpty()) {
            userInformation = userDetails.get(0);
        } else {
            throw new Exception("No user details given.");
        }
    }

    @When("^User sent \"(.*)\" request")
    public void sendRequest(String requestName) throws Exception {
        ISOAPSender sender = new SOAPSender(url);
        Document request = messageFactory.loadMessageFromResource("requests/" + requestName + ".xml");
        soapMessageHelper.setAttribute(request, "//~BookRemoteRef~", "Name", userInformation.getBookRemoteReference());
        soapMessageHelper.setAttribute(request, "//~BookRemoteRef~", "Value", userInformation.getPnr());
        soapMessageHelper.setAttribute(request, "//~GuestCriteria~", "LastName", userInformation.getGuestLastName());
        //response = sender.sendSOAPMessage(request);
    }

    @Then("^Response (should not|should) have (.*)")
    public void checkResponseText(@Transform(BooleanTransformation.class) boolean isPresent, String text) throws Exception {
        ISOAPMessageConverter messageConverter = new SOAPMessageConverter();
        String responseAsText = messageConverter.convertNodeToString(response);
        if (isPresent) {
            Assert.assertTrue(responseAsText.contains(text));
        } else {
            Assert.assertFalse(responseAsText.contains(text));
        }
    }
}