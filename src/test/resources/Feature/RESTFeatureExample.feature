Feature: As a user I want to send low fare search request.

  Background: Set basic parameters.
    Given URI is "http://10.160.7.156/tdprest-2/api"

  Scenario Outline: Create a Air Low Fare Search with one traveler, one-way flight
    Given User has flight from <From> to <To> <DaysFromToday> days from today
    When User send LFS request
    Then Verify response
  @smoke
    Examples:
      | DaysFromToday | From | To  |
      | 60            | JFK  | BOS |
  @regression
    Examples:
      | DaysFromToday | From | To  |
      | 35            | JFK  | BOS |
      | 42            | BOS  | JFK |