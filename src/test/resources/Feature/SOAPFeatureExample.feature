Feature: As a user I want to be able to check if a reservation is valid.

  Background: Set basic parameters.
    Given Url is "http://10.160.7.131:8080/soap/OTA/com/datalex/m3/M3.jws"

  @smoke
  Scenario: Correct response should be returned if reservation is valid.
    Given User has entered his reservation details.
      | pnr    | guestLastName | bookRemoteReference |
      | HOYMFZ | THOMAS        | SABRE_SECURE        |
    When User sent "ReservationRetrieveResynchSv" request
    Then Response should not have errors.