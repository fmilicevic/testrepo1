Feature: As a user I want to be able to complete booking

  @smoke
  Scenario: Correct reservation number should be returned after booking is complete.
    Given User has populated FlightSearch Page
    And User populates Selection Page
    And User populates Summary Page
    And User populates Travellers Page
    When User purchase flight
    Then User get Confirmation Page