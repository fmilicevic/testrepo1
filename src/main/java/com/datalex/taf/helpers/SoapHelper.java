package com.datalex.taf.helpers;

import com.datalex.taf.core.loggers.TAFLogger;
import com.datalex.taf.core.readers.property.LoadProperties;
import com.datalex.taf.core.readers.property.TAFProperties;
import com.datalex.taf.soap.converters.SOAPMessageConverter;
import com.datalex.taf.soap.helpers.SOAPMessageHelper;
import com.datalex.taf.soap.sender.ISOAPSender;
import com.datalex.taf.soap.sender.SOAPSender;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

import com.datalex.taf.soap.helpers.SOAPMessageHelper;

/**
 * Local SOAP Helper
 */
public class SoapHelper {

    private static SOAPMessageHelper soapMessageHelper = new SOAPMessageHelper();
    /**
     * Function to convert document to file
     *
     * @param doc      response that we want to convert
     * @param fileName new file name for xml response
     */
    static void convertDocumentToFile(Document doc, String fileName) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        Result output = new StreamResult(new File(fileName));
        Source input = new DOMSource(doc);
        transformer.transform(input, output);
        System.out.println("Document is converted to file!");
    }

    /**
     * Function to create document from file
     *
     * @param fileName xml file path
     * @return Document object
     */
    public static Document createDocumentFromFile(String fileName) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        InputSource is = new InputSource(fileName);
        is.setEncoding("UTF-8");
        Document outputDoc = dbFactory.newDocumentBuilder().parse(is);
        System.out.println("Created document from file: " + fileName);
        return outputDoc;
    }

    /**
     * Function to return project ip
     *
     * @return project ip
     */
    public static String returnProjectIp() throws IOException {
        LoadProperties.propertyLoader();
        return TAFProperties.getPROJECTIP();
    }

    /**
     * Function to convert xml to doc
     *
     * @param xmlPath xml file path
     * @return Document object
     */
    public static Document convertXmlToDoc(String xmlPath) throws Exception {
        File file = new File(xmlPath);
        String message = FileUtils.readFileToString(file, "UTF-8");
        System.out.println("xml is converted to UTF-8");
        SOAPMessageConverter converter = new SOAPMessageConverter();
        System.out.println("File is converted to doc!");
        return converter.convertStringToDocument(message);
    }

    /**
     * Function to send xml Soap request
     *
     * @param reqPath  xml file path
     * @param url      url for sending request
     * @param response name for xml file response
     * @return Document object
     */
    public static Document sendXmlRequest(String reqPath, String url, String response) throws Throwable {
        Document doc, docResponse;
        doc = convertXmlToDoc(reqPath);
        ISOAPSender sender = new SOAPSender(url);
        System.out.println("Sending XML Request...");
        docResponse = sender.sendSOAPMessage(doc);
        System.out.println("Request is sent!");
        String newFileName = "work/" + response + ".xml";
        convertDocumentToFile(docResponse, newFileName);
        System.out.println("Response file location: " + newFileName);
        assertNoErrors(docResponse);
        return docResponse;
    }

    /**
     * Function assert are there errors present in response
     *
     * @param document document
     */
    private static void assertNoErrors(Document document) throws Exception {
        String documentAsString = soapMessageHelper.convertToString(document);
        String message = "Response contains ERRORS!";
        Assert.assertFalse(message + "; Message contains errors", documentAsString.toLowerCase().contains("error"));
        Assert.assertFalse(message + "; Message contains Stacktrace information", documentAsString.toLowerCase().contains("stacktrace"));
    }

    /**
     * Function to change parameter in request
     *
     * @param rqDoc      request doc
     * @param paramXpath xpath to change
     * @param value      value to set
     * @return Document object
     */
    public static Document changeParamInRQ(Document rqDoc, String paramXpath, String value) throws Exception {
        TAFLogger.tafDebug("Changing parameter: " + paramXpath + " to " + value);
        NodeList nodes = soapMessageHelper.evaluateXpathInXml(rqDoc, paramXpath, null);
        if (nodes.getLength() == 0) {
            throw new Exception("ChangeParamInRQ failed: couldn\'t find xpath " + paramXpath + " in Document");
        } else {
            for (int doc = 0; doc < nodes.getLength(); ++doc) {
                nodes.item(doc).setNodeValue(value);
            }

            return rqDoc;
        }
    }

    /**
     * Function to change parameter in request
     *
     * @param doc      document
     * @param filePath file path for saving document
     */
    public static void saveDocumentToFile(Document doc, String filePath) throws Exception {
        doc.normalize();
        Transformer xformer = TransformerFactory.newInstance().newTransformer();
        xformer.setOutputProperty("indent", "yes");
        xformer.transform(new DOMSource(doc), new StreamResult(new File(filePath)));
        System.out.println("Document is saved at: " + filePath);
    }
}