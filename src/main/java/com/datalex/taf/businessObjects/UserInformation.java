package com.datalex.taf.businessObjects;


public class UserInformation {
    private String pnr;
    private String guestLastName;
    private String bookRemoteReference;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getGuestLastName() {
        return guestLastName;
    }

    public void setGuestLastName(String guestLastName) {
        this.guestLastName = guestLastName;
    }

    public String getBookRemoteReference() {
        return bookRemoteReference;
    }

    public void setBookRemoteReference(String bookRemoteReference) {
        this.bookRemoteReference = bookRemoteReference;
    }
}