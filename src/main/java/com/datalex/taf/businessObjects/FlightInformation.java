package com.datalex.taf.businessObjects;

public class FlightInformation {
    private String from;
    private String to;
    private int daysFromToday;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getDaysFromToday() {
        return daysFromToday;
    }

    public void setDaysFromToday(int daysFromToday) {
        this.daysFromToday = daysFromToday;
    }
}
