package com.datalex.taf.transformations;

import cucumber.api.Transformer;

public class BooleanTransformation extends Transformer<Boolean> {

    public Boolean transform(String value){
        if(value.equals("should")){
            return true;
        }else if(value.equals("should not")){
            return false;
        }
        return false;
    }
}
